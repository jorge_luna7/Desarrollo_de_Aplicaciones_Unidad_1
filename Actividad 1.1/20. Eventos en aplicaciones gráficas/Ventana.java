/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventos;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.event.ActionListener;
/**
 *
 * @author Jorge Alberto Luna Vasquez
 */
public class Ventana extends JFrame implements ActionListener {
 JPanel panel;
 JButton boton,boton2;
 JLabel texto;
 JTextField input;
 
    public Ventana(){
 panel = new JPanel();
 boton = new JButton("Has click aqui");
  boton2 = new JButton("Has click aqui");
 texto = new JLabel();
 input= new JTextField("Escribe aqui");
 this.add(panel);
 panel.add(boton);
 panel.add(boton2);
 panel.add(input);
 panel.add(texto);
 this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
 
 this.boton.addActionListener(this);
  this.boton2.addActionListener(this);
 }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(boton)) {
                this.texto.setText(input.getText());
        }
  if (e.getSource().equals(boton2)) {
                this.texto.setText("Haz oprimido el boton 2");
        }
    }
    
}
