/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instruccion_while;

/**
 *
 * @author Jorge Alberto Luna Vasquez
 */
public class Instruccion_while {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int var = 0;
        while (var <= 15) {
            System.out.println("El valor de la variable es: " + var);
            var = var + 3;
        }

    }

}
