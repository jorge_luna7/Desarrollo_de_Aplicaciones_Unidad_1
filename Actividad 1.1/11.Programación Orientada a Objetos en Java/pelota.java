/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacion_orientada_a_objetos;

/**
 *
 * @author Jorge Alberto Luna Vasquez
 */
public class pelota {
    float radio;
    float peso;
    public pelota(){
        radio=100;
        peso=250;
    }
    public float getRadio() {
        return radio;
    }
    public void patearPelota(){
        System.out.println("Haz pateado la pelota.");
    }
    public void atraparPelota(){
        System.out.println("Haz atrapado la pelota.");
    }

    public void setRadio(float radio) {
        this.radio = radio;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }
    public pelota(float radio, float peso) {
        this.radio = radio;
        this.peso = peso;
    }

}
