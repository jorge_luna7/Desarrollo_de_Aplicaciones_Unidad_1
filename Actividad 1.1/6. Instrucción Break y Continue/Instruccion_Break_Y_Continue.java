/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instruccion_break_y_continue;

/**
 *
 * @author Jorge Alberto Luna Vasquez
 */
public class Instruccion_Break_Y_Continue {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            for (int i = 0; i < 10; i++) {
                System.out.println("Aun estas en el ciclo");
                if (i==1) {
                    System.out.println("Regresamos al ciclo");
                    continue;
                }
                if (i==4) {
                    break;
                }
                System.out.println("El valor de i es: "+i);
                
        }
            System.out.println("Has dejado el ciclo for");
    }
    
}
